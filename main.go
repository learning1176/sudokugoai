package main

import (
	"fmt"
	"os"
	"sudokugoai/logic"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Expected at least 1 argument, got %d!\n", len(os.Args)-1)
		os.Exit(1)
	}

	sudoku, err := logic.LoadSudokuFromFile(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error loading sudoku file: %s\n", err)
		os.Exit(1)
	}

	fmt.Println("Input sudoku:")
	logic.DisplaySudoku(sudoku)

	selector := logic.NewSmartSelector()
	sequence_loader := logic.NewSelectorSequenceLoader(logic.NewPrimitiveSelector())
	sudoku_solver := logic.NewSudokuSolver(selector, sequence_loader)

	solution := sudoku_solver.SolveSudoku(sudoku)
	if solution.IsSolved {
		fmt.Printf("Solution: (%d elements assigned)\n", solution.StepCount)
		logic.DisplaySudoku(solution.Sudoku)
	} else {
		fmt.Printf("Could not solve this sudoku. (%d elements assigned)\n", solution.StepCount)
		logic.DisplaySudoku(solution.Sudoku)
	}
}
