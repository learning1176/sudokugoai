package logic

import (
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func parse_sudoku_sector_dimension_line(line string) (int, int, error) {
	parts := strings.Split(line, ";")
	if len(parts) != 2 {
		return 0, 0, fmt.Errorf("exactly 2 numbers expected in dimension line, got %d", len(parts))
	}

	sw, err := strconv.Atoi(parts[0])
	if err != nil {
		return 0, 0, fmt.Errorf("%s could not be parsed to number", parts[0])
	}
	if sw <= 0 {
		return 0, 0, fmt.Errorf("sector width must be positive number, got %d", sw)
	}

	sh, err := strconv.Atoi(parts[1])
	if err != nil {
		return 0, 0, fmt.Errorf("%s could not be parsed to number", parts[1])
	}
	if sh <= 0 {
		return 0, 0, fmt.Errorf("sector height must be positive number, got %d", sh)
	}

	return sw, sh, nil
}

func parse_row(row string, size int) ([]int, error) {
	parts := strings.Split(row, ";")
	if len(parts) != size {
		return nil, fmt.Errorf("expected %d elements in a row, got %d", size, len(parts))
	}

	elements := make([]int, size)
	for i, el := range parts {
		n, err := strconv.Atoi(el)
		if err != nil {
			return nil, fmt.Errorf("%s could not be parsed to number", el)
		}
		if n < 0 || n > size {
			return nil, fmt.Errorf("%d can not be a number in sudoku of size %d", n, size)
		}
		elements[i] = n
	}

	return elements, nil
}

func LoadSudokuFromFile(path string) (*Sudoku, error) {
	dat, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	lines := strings.Split(string(dat), "\n")
	if len(lines) < 1 {
		return nil, errors.New("the sudoku file does not contain dimension info line")
	}

	sw, sh, err := parse_sudoku_sector_dimension_line(lines[0])
	if err != nil {
		return nil, err
	}
	size := sw * sh

	if len(lines) != size+1 {
		return nil, fmt.Errorf("exactly %d lines for row specification expected in sudoku file, got %d", size, len(lines)-1)
	}

	elements := make([][]int, size)
	for i := 0; i < size; i++ {
		row, err := parse_row(lines[i+1], size)
		if err != nil {
			return nil, fmt.Errorf("line %d: %s", i+2, err)
		}
		elements[i] = row
	}

	return &Sudoku{
		Size:         size,
		SectorWidth:  sw,
		SectorHeight: sh,
		Elements:     elements,
	}, nil
}

func DisplaySudoku(s *Sudoku) {
	element_width := int(math.Log10(float64(s.Size))) + 2
	blank_element := ""
	for i := 0; i < element_width; i++ {
		blank_element += " "
	}
	element_format := fmt.Sprintf("%s%d%s", "%", element_width, "d")

	line_length := s.Size*element_width + s.Size/s.SectorWidth*2 + 1
	line := ""
	for i := 0; i < line_length; i++ {
		line += "-"
	}

	for x := 0; x < s.Size; x++ {
		if x%s.SectorHeight == 0 {
			fmt.Println(line)
		}

		fmt.Printf("|")
		for y := 0; y < s.Size; y++ {
			if y%s.SectorWidth == 0 && y > 0 {
				fmt.Printf(" |")
			}
			if s.Elements[x][y] == 0 {
				fmt.Print(blank_element)
			} else {
				fmt.Printf(element_format, s.Elements[x][y])
			}
		}
		fmt.Printf(" |\n")
	}
	fmt.Println(line)
}
