package logic

type element_checker func(*Sudoku, int, int) bool

type Selector struct {
	element_checker element_checker
}

func (selector *Selector) get_possible_elements(s *Sudoku, x int, y int) []int {
	elements := make([]int, 0)
	original := s.Elements[x][y]

	for i := 1; i <= s.Size; i++ {
		s.Elements[x][y] = i
		if selector.element_checker(s, x, y) {
			elements = append(elements, i)
		}
	}

	s.Elements[x][y] = original
	return elements
}

func NewPrimitiveSelector() *Selector {
	return &Selector{
		element_checker: func(s *Sudoku, x int, y int) bool {
			return true
		},
	}
}

func NewRowAwareSelector() *Selector {
	return &Selector{
		element_checker: func(s *Sudoku, x int, y int) bool {
			return s.is_row_non_conflicting(x)
		},
	}
}

func NewColumnAwareSelector() *Selector {
	return &Selector{
		element_checker: func(s *Sudoku, x int, y int) bool {
			return s.is_column_non_conflicting(y)
		},
	}
}

func NewSectorAwareSelector() *Selector {
	return &Selector{
		element_checker: func(s *Sudoku, x int, y int) bool {
			return s.is_sector_by_elm_pos_non_conflicting(x, y)
		},
	}
}

func NewCompositeSelector(selectors []*Selector) *Selector {
	return &Selector{
		element_checker: func(s *Sudoku, x int, y int) bool {
			for _, selector := range selectors {
				if !selector.element_checker(s, x, y) {
					return false
				}
			}
			return true
		},
	}
}

func NewSmartSelector() *Selector {
	return NewCompositeSelector([]*Selector{
		NewRowAwareSelector(),
		NewColumnAwareSelector(),
		NewSectorAwareSelector(),
	})
}
