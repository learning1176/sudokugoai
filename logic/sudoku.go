package logic

type Sudoku struct {
	Size         int
	SectorWidth  int
	SectorHeight int
	Elements     [][]int
}

func are_non_conflicting(elems []int) bool {
	elem_occurences := make([]int, len(elems)+1)
	for _, elem := range elems {
		elem_occurences[elem]++
	}

	for i := 1; i < len(elem_occurences); i++ {
		if elem_occurences[i] > 1 {
			return false
		}
	}

	return true
}

func (s *Sudoku) get_row(x int) []int {
	return s.Elements[x]
}

func (s *Sudoku) get_column(y int) []int {
	column := make([]int, s.Size)
	for i := 0; i < s.Size; i++ {
		column[i] = s.Elements[i][y]
	}
	return column
}

func (s *Sudoku) is_row_non_conflicting(x int) bool {
	return are_non_conflicting(s.get_row(x))
}

func (s *Sudoku) is_column_non_conflicting(y int) bool {
	return are_non_conflicting(s.get_column(y))
}

func (s *Sudoku) get_sector(sx int, sy int) []int {
	sector := make([]int, s.Size)
	sector_pos := 0

	for x := 0; x < s.SectorHeight; x++ {
		for y := 0; y < s.SectorWidth; y++ {
			sector[sector_pos] = s.Elements[sx*s.SectorHeight+x][sy*s.SectorWidth+y]
			sector_pos++
		}
	}

	return sector
}

func (s *Sudoku) get_sector_by_elm_pos(ex int, ey int) []int {
	return s.get_sector(ex/s.SectorHeight, ey/s.SectorWidth)
}

func (s *Sudoku) is_sector_non_conflicting(sx int, sy int) bool {
	return are_non_conflicting(s.get_sector(sx, sy))
}

func (s *Sudoku) is_sector_by_elm_pos_non_conflicting(ex int, ey int) bool {
	return are_non_conflicting(s.get_sector_by_elm_pos(ex, ey))
}

func (s *Sudoku) is_full() bool {
	for x := 0; x < s.Size; x++ {
		for y := 0; y < s.Size; y++ {
			if s.Elements[x][y] == 0 {
				return false
			}
		}
	}

	return true
}

func (s *Sudoku) is_solved() bool {
	if !s.is_full() {
		return false
	}

	for i := 0; i < s.Size; i++ {
		if !s.is_row_non_conflicting(i) {
			return false
		}
	}

	for i := 0; i < s.Size; i++ {
		if !s.is_column_non_conflicting(i) {
			return false
		}
	}

	for x := 0; x < s.Size/s.SectorHeight; x++ {
		for y := 0; y < s.Size/s.SectorWidth; y++ {
			if !s.is_sector_non_conflicting(x, y) {
				return false
			}
		}
	}

	return true
}

func (s Sudoku) copy() *Sudoku {
	es := make([][]int, s.Size)
	for i := 0; i < s.Size; i++ {
		es[i] = make([]int, s.Size)
		copy(es[i], s.Elements[i])
	}

	s.Elements = es
	return &s
}
