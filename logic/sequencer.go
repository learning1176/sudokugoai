package logic

import (
	"sort"
)

type Position struct {
	x int
	y int
}

func NewPos(x int, y int) *Position {
	return &Position{
		x: x,
		y: y,
	}
}

type Sequencer struct {
	positions       []*Position
	current_pos     int
	sequence_loader SequenceLoader
}

func NewSequencer(sequence_loader SequenceLoader) *Sequencer {
	return &Sequencer{
		sequence_loader: sequence_loader,
	}
}

type SequenceLoader interface {
	load_sequence(s *Sudoku) []*Position
}

func (sequencer *Sequencer) init(s *Sudoku) {
	sequencer.current_pos = -1
	sequencer.positions = sequencer.sequence_loader.load_sequence(s)
}

func (sequencer *Sequencer) next() *Position {
	sequencer.current_pos++
	if sequencer.current_pos >= len(sequencer.positions) {
		sequencer.current_pos = len(sequencer.positions)
		return nil
	}
	return sequencer.positions[sequencer.current_pos]
}

func (sequencer *Sequencer) prev() *Position {
	sequencer.current_pos--
	if sequencer.current_pos <= -1 {
		sequencer.current_pos = -1
		return nil
	}
	return sequencer.positions[sequencer.current_pos]
}

type PrimitiveSequenceLoader struct {
}

func NewPrimitiveSequenceLoader() *PrimitiveSequenceLoader {
	return &PrimitiveSequenceLoader{}
}

func (psl *PrimitiveSequenceLoader) load_sequence(s *Sudoku) []*Position {
	positions := make([]*Position, 0)

	for x := 0; x < s.Size; x++ {
		for y := 0; y < s.Size; y++ {
			if s.Elements[x][y] == 0 {
				positions = append(positions, NewPos(x, y))
			}
		}
	}

	return positions
}

type SelectorSequenceLoader struct {
	selector *Selector
}

func NewSelectorSequenceLoader(selector *Selector) *SelectorSequenceLoader {
	return &SelectorSequenceLoader{
		selector: selector,
	}
}

type PositionOptions struct {
	position *Position
	options  []int
}

func (ssl *SelectorSequenceLoader) load_sequence(s *Sudoku) []*Position {
	position_options := make([]*PositionOptions, 0)

	for x := 0; x < s.Size; x++ {
		for y := 0; y < s.Size; y++ {
			if s.Elements[x][y] == 0 {
				position_options = append(position_options, &PositionOptions{
					position: &Position{
						x: x,
						y: y,
					},
					options: ssl.selector.get_possible_elements(s, x, y),
				})
			}
		}
	}

	sort.Slice(position_options, func(i, j int) bool {
		return len(position_options[i].options) < len(position_options[j].options)
	})

	positions := make([]*Position, len(position_options))
	for i, position_option := range position_options {
		positions[i] = position_option.position
	}
	return positions
}
