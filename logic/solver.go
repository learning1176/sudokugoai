package logic

type SudokuSolver struct {
	selector        *Selector
	sequence_loader SequenceLoader
}

type SudokuSolution struct {
	Sudoku    *Sudoku
	Sequencer *Sequencer
	IsSolved  bool
	StepCount int
}

func NewSudokuSolver(selector *Selector, sequence_loader SequenceLoader) *SudokuSolver {
	return &SudokuSolver{
		selector:        selector,
		sequence_loader: sequence_loader,
	}
}

func (ss *SudokuSolver) SolveSudoku(s *Sudoku) *SudokuSolution {
	sudoku_solution := &SudokuSolution{
		Sudoku:    s.copy(),
		Sequencer: NewSequencer(ss.sequence_loader),
		IsSolved:  false,
		StepCount: 0,
	}

	sudoku_solution.Sequencer.init(sudoku_solution.Sudoku)
	sudoku_solution.IsSolved = ss.solve_sudoku_next_pos(sudoku_solution)

	return sudoku_solution
}

func (ss *SudokuSolver) solve_sudoku_next_pos(sudoku_solution *SudokuSolution) bool {
	pos := sudoku_solution.Sequencer.next()
	var solved bool
	if pos == nil {
		solved = sudoku_solution.Sudoku.is_solved()
	} else {
		sudoku_solution.StepCount++
		solved = ss.solve_sudoku_pos(sudoku_solution, pos)
	}

	sudoku_solution.Sequencer.prev()
	return solved
}

func (ss *SudokuSolver) solve_sudoku_pos(sudoku_solution *SudokuSolution, pos *Position) bool {
	elements := ss.selector.get_possible_elements(sudoku_solution.Sudoku, pos.x, pos.y)
	pos_pointer := &sudoku_solution.Sudoku.Elements[pos.x][pos.y]

	return ss.try_elements_for_pos(sudoku_solution, pos_pointer, elements)
}

func (ss *SudokuSolver) try_elements_for_pos(sudoku_solution *SudokuSolution, pos_pointer *int, elements []int) bool {
	for _, elem := range elements {
		*pos_pointer = elem
		if ss.solve_sudoku_next_pos(sudoku_solution) {
			return true
		}
	}
	*pos_pointer = 0

	return false
}
